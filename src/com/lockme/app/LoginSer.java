package com.lockme.app;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

public class LoginSer {

	public void saveCredentials(String mainUserName, String lockerFileName) throws IOException {

		Scanner scannerObject = new Scanner(System.in);
		while (true) {
			System.out.print("Please enter site name :: ");
			String sitename = scannerObject.next();
			System.out.print("Please enter Username :: ");
			String username = scannerObject.next();
			System.out.print("Please enter Password :: ");
			String password = scannerObject.next();

			String content = mainUserName + "," + sitename + "," + username + "," + password;
			try (FileWriter fw = new FileWriter(lockerFileName, true)) {
				fw.write(content + "\n");
			}

			System.out.println("Saved credentials successfully.");

			System.out.println("continue or exit..");
			String option = scannerObject.next();

			if (!option.contentEquals("continue")) {
				scannerObject.close();
				break;
			}
		}

	}

	public void retriveCredentials(String mainUserName, String lockerFileName) throws IOException {
		Boolean[] isDataRetrive = { false };
		try (Stream<String> stream = Files.lines(Paths.get(lockerFileName))) {
			stream.forEach(line -> {
				String data[] = line.split(",");
				if (mainUserName.contentEquals(data[0])) {
					System.out.println(line);
					isDataRetrive[0] = true;
				}
			});
		}
	}
}